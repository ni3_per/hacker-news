// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_failure.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<ResponseFailure> _$responseFailureSerializer =
    new _$ResponseFailureSerializer();

class _$ResponseFailureSerializer
    implements StructuredSerializer<ResponseFailure> {
  @override
  final Iterable<Type> types = const [ResponseFailure, _$ResponseFailure];
  @override
  final String wireName = 'ResponseFailure';

  @override
  Iterable<Object?> serialize(Serializers serializers, ResponseFailure object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.message;
    if (value != null) {
      result
        ..add('message')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.applicationStatusCode;
    if (value != null) {
      result
        ..add('applicationStatusCode')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.httpStatus;
    if (value != null) {
      result
        ..add('httpStatus')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ResponseFailure deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ResponseFailureBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'applicationStatusCode':
          result.applicationStatusCode = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'httpStatus':
          result.httpStatus = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$ResponseFailure extends ResponseFailure {
  @override
  final String? message;
  @override
  final int? applicationStatusCode;
  @override
  final int? httpStatus;

  factory _$ResponseFailure([void Function(ResponseFailureBuilder)? updates]) =>
      (new ResponseFailureBuilder()..update(updates)).build();

  _$ResponseFailure._(
      {this.message, this.applicationStatusCode, this.httpStatus})
      : super._();

  @override
  ResponseFailure rebuild(void Function(ResponseFailureBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ResponseFailureBuilder toBuilder() =>
      new ResponseFailureBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ResponseFailure &&
        message == other.message &&
        applicationStatusCode == other.applicationStatusCode &&
        httpStatus == other.httpStatus;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, message.hashCode), applicationStatusCode.hashCode),
        httpStatus.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ResponseFailure')
          ..add('message', message)
          ..add('applicationStatusCode', applicationStatusCode)
          ..add('httpStatus', httpStatus))
        .toString();
  }
}

class ResponseFailureBuilder
    implements Builder<ResponseFailure, ResponseFailureBuilder> {
  _$ResponseFailure? _$v;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  int? _applicationStatusCode;
  int? get applicationStatusCode => _$this._applicationStatusCode;
  set applicationStatusCode(int? applicationStatusCode) =>
      _$this._applicationStatusCode = applicationStatusCode;

  int? _httpStatus;
  int? get httpStatus => _$this._httpStatus;
  set httpStatus(int? httpStatus) => _$this._httpStatus = httpStatus;

  ResponseFailureBuilder();

  ResponseFailureBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _message = $v.message;
      _applicationStatusCode = $v.applicationStatusCode;
      _httpStatus = $v.httpStatus;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ResponseFailure other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ResponseFailure;
  }

  @override
  void update(void Function(ResponseFailureBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ResponseFailure build() {
    final _$result = _$v ??
        new _$ResponseFailure._(
            message: message,
            applicationStatusCode: applicationStatusCode,
            httpStatus: httpStatus);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
