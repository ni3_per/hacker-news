import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'response_failure.g.dart';


abstract class ResponseFailure implements Built<ResponseFailure,ResponseFailureBuilder>
{
  String? get message;
  int? get applicationStatusCode;
  int? get httpStatus;

ResponseFailure._();

factory ResponseFailure([updates(ResponseFailureBuilder b)/*!*/]) = _$ResponseFailure;

static Serializer<ResponseFailure> get serializer => _$responseFailureSerializer;
}