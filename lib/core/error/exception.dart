import 'package:built_value/built_value.dart';

class ServerException implements Exception {
  @nullable
  final String? message;
  @nullable
  final int? applicationStatusCode;

  ServerException([this.message, this.applicationStatusCode]);

  String toString() => "$message";
}

class CacheException implements Exception {}

class FetchDataException extends ServerException {
  FetchDataException([String? message]) : super(message);
}

class BadRequestException extends ServerException {
  BadRequestException([message, applicationStatusCode])
      : super(message, applicationStatusCode);
}

class UnauthorisedException extends ServerException {
  UnauthorisedException([message, applicationStatusCode])
      : super(message, applicationStatusCode);
}

class InvalidInputException extends ServerException {
  InvalidInputException([String? message]) : super(message);
}
