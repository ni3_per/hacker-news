import 'package:hacker_news/core/index.dart';
import 'package:built_value/built_value.dart';
import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  @override
  List<Object?> get props => [];
}

class ServerFailure extends Failure {
  final String? message;

  @nullable
  final int? applicationStatusCode;

  ServerFailure([this.message, this.applicationStatusCode]);
  @override
  List<Object?> get props => [message];
}

class NetworkFailure extends Failure {
  @override
  List<Object> get props => [props];
  @override
  String toString() => AppStrings.message.kNoInternet;
}

class CacheFailure extends Failure {
  @override
  List<Object> get props => [];
}
