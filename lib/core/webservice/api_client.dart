import 'package:hacker_news/core/index.dart';

import 'package:chopper/chopper.dart';
import 'package:hacker_news/ui/search_news/model/news.dart';
import 'package:hacker_news/ui/search_news/model/news_list.dart';

part 'api_client.chopper.dart';

@ChopperApi(baseUrl: "")
abstract class ApiClient extends ChopperService {
  /// Search news
  @Get(path: APIs.searchNews)
  Future<Response<NewsList>> searchNews(@Path('query') String query);

  /// Fetch News Details
  @Get(path: APIs.fetchNewsDetails)
  Future<Response<News>> fetchNewsDetails(@Path('news_id') String newsID);

  static ApiClient create() {
    final client = ChopperClient(
      baseUrl: APIs.apiBaseUrl,
      services: [_$ApiClient()],
      converter: BuiltValueConverter(),
      errorConverter: BuiltValueConverter(errorType: ResponseFailure),
      interceptors: [
        HttpLoggingInterceptor(),
        NetworkInterceptor(),
        ExceptionInterceptor(),
        HeaderInterceptor()
      ],
    );
    return _$ApiClient(client);
  }
}
