import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:hacker_news/core/index.dart';
import 'package:chopper/chopper.dart';

class NetworkInterceptor implements RequestInterceptor {
  @override
  FutureOr<Request> onRequest(Request request) async {
    final connectivityResult = await Connectivity().checkConnectivity();

    final isInternetAvailable =
        connectivityResult == ConnectivityResult.mobile ||
            connectivityResult == ConnectivityResult.wifi;

    log.info('--------isInternetAvailable------- $isInternetAvailable');

    if (!isInternetAvailable) {
      throw NoConnectivityException();
    } else {}
    return request;
  }
}

class NoConnectivityException implements Exception {
  @override
  String toString() => AppStrings.message.kNoInternet;
}
