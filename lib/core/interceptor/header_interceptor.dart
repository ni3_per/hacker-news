import 'dart:async';

import 'package:chopper/chopper.dart';

class HeaderInterceptor implements RequestInterceptor {
  static const String authHeader = "Authorization";
  static const String bearer = "Bearer ";

  @override
  FutureOr<Request> onRequest(Request request) async {
    return request;
  }
}
