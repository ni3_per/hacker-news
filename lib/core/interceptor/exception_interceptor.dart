import 'dart:async';

import 'package:hacker_news/core/index.dart';
import 'package:chopper/chopper.dart';

class ExceptionInterceptor implements ResponseInterceptor {
  @override
  FutureOr<Response> onResponse(Response response) {
    switch (response.statusCode) {
      case 200:
      case 201:
        return response;
      case 400:
        final ResponseFailure exception = response.error as ResponseFailure;

        throw BadRequestException(
            exception.message, exception.applicationStatusCode);
      case 401:
      case 403:
        final ResponseFailure exception = response.error as ResponseFailure;

        throw UnauthorisedException(
            exception.message, exception.applicationStatusCode);
      case 500:
      default:
        throw FetchDataException(
            'Error occurred while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
