import 'package:hacker_news/core/index.dart';

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:hacker_news/ui/search_news/model/comment.dart';
import 'package:hacker_news/ui/search_news/model/news.dart';
import 'package:hacker_news/ui/search_news/model/news_list.dart';
import 'package:built_collection/built_collection.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  ResponseFailure,
  NewsList,
  News,
  Comment,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
