import 'package:chopper/chopper.dart';

class Singleton {
  Singleton._();
  static final Singleton _instance = Singleton._();

  factory Singleton() => _instance;

  late String _firebaseToken;

  late ChopperClient _apiClient;

  void setFirebaseToken(String token) {
    _firebaseToken = token;
  }

  String? getFirebaseToken() => _firebaseToken;

  void setApiClinetInstance(ChopperClient apiClient) {
    this._apiClient = apiClient;
  }

  ChopperClient getApiClinetInstance() => this._apiClient;
}
