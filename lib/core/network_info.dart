import 'dart:async';

import 'package:hacker_news/core/util/logger.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/services.dart';

class NetworkInfo {
  bool isConnected = false;
  final Connectivity _connectivity = Connectivity();
  StreamSubscription<ConnectivityResult>? _connectivitySubscription;

  static final NetworkInfo _singleton = NetworkInfo._internal();

  factory NetworkInfo() => _singleton;

  NetworkInfo._internal() {
    _initConnectivity();
    _connectivitySubscription =
        _connectivity.onConnectivityChanged.listen(_updateConnectionStatus);
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initConnectivity() async {
    ConnectivityResult result = ConnectivityResult.none;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      result = await _connectivity.checkConnectivity();
    } on PlatformException catch (e) {
      log.info(e.toString());
    }

    return _updateConnectionStatus(result);
  }

  Future<void> _updateConnectionStatus(ConnectivityResult result) async {
    switch (result) {
      case ConnectivityResult.wifi:
      case ConnectivityResult.mobile:
        isConnected = true;
        break;
      case ConnectivityResult.none:
        isConnected = false;
        break;
      default:
        isConnected = false;
        break;
    }
  }

  void cancelSubscription() {
    _connectivitySubscription?.cancel();
  }
}
