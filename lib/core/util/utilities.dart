import 'package:hacker_news/ui/search_news/entities/comment_info.dart';
import 'package:hacker_news/ui/search_news/model/comment.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

import '../index.dart';

class Utilities {
  static List<CommentInfo> getListOfCommentInfo(List<Comment>? comments) {
    List<CommentInfo> _tempList = [];
    comments?.forEach((comment) {
      if (comment.author != null && comment.text != null) {
        _tempList.add(comment.toEntity());
      }
    });

    return _tempList;
  }

  static String getTimeAgoString(String dateString) {
    return timeago.format(
      Utilities.getDateTimeFrom(
        dateString: dateString,
        format: AppStrings.dateFormats.kFullUTCDateTime,
        isUTC: true,
      ),
    );
  }

  static String convertDateString({
    String dateString = '',
    String? fromFormat,
    String? toFormat,
    bool fromUTC = false,
    bool toLocal = false,
  }) {
    if (dateString.isEmpty) {
      return '';
    }
    DateTime date = getDateTimeFrom(
        dateString: dateString, format: fromFormat, isUTC: fromUTC);
    if (toLocal) {
      date = date.toLocal();
    }
    String convertedDateString =
        getDateStringFrom(date: date, format: toFormat);
    return convertedDateString;
  }

  static DateTime getDateTimeFrom(
      {String? dateString, String? format, bool? isUTC}) {
    DateFormat dateFormat = DateFormat(format);
    DateTime date =
        dateFormat.parse(dateString!, (isUTC! == true) ? isUTC : false);
    return date;
  }

  static String getDateStringFrom(
      {DateTime? date, String? format, bool isUTC = false}) {
    DateFormat dateFormat = DateFormat(format);
    String formattedDate = dateFormat.format(date!);
    if (isUTC) {
      DateTime parsedDate = dateFormat
          .parse(formattedDate, (isUTC == true) ? isUTC : false)
          .toLocal();
      String dateLocal = dateFormat.format(parsedDate);
      return dateLocal;
    } else {
      return formattedDate;
    }
  }
}
