import 'package:built_collection/built_collection.dart';
import 'package:chopper/chopper.dart';

import '../index.dart';

class BuiltValueConverter extends JsonConverter {
  final Type? errorType;

  BuiltValueConverter({this.errorType});

  @override
  Request convertRequest(Request request) => super.convertRequest(
        request.copyWith(
          body: serializers.serializeWith(
            serializers.serializerForType(request.body.runtimeType)!,
            request.body,
          ),
        ),
      );

  @override
  Response<BodyType> convertResponse<BodyType, SingleItemType>(
    Response response,
  ) {
    final Response dynamicResponse = super.convertResponse(response);
    final BodyType? customBody =
        convertToCustomObject<SingleItemType>(dynamicResponse.body);
    return dynamicResponse.copyWith<BodyType>(body: customBody);
  }

  @override
  Response convertError<BodyType, SingleItemType>(Response response) {
    final dynamicResponse = super.convertResponse(response);
    var body;
    if (errorType != null) {
      final serializer = serializers.serializerForType(errorType!)!;
      body = serializers.deserializeWith(serializer, dynamicResponse.body);
    }
    body ??= dynamicResponse.body;
    return dynamicResponse.copyWith(body: body);
  }

  dynamic convertToCustomObject<SingleItemType>(dynamic element) {
    if (element is SingleItemType) return element;

    if (element is List) {
      return deserializeListOf<SingleItemType>(element);
    } else {
      return _deserialize<SingleItemType>(element);
    }
  }

  BuiltList<SingleItemType> deserializeListOf<SingleItemType>(
    List dynamicList,
  ) {
    if (dynamicList.isEmpty ||
        dynamicList.first is String ||
        dynamicList.first is int ||
        dynamicList.first is double) {
      return BuiltList<SingleItemType>(
        dynamicList.map((element) => element),
      );
    } else {
      return BuiltList<SingleItemType>(
        dynamicList.map((element) => _deserialize<SingleItemType>(element)),
      );
    }
  }

  SingleItemType _deserialize<SingleItemType>(
    Map<String, dynamic> value,
  ) =>
      serializers.deserializeWith(
        serializers.serializerForType(SingleItemType)!,
        value,
      );
}
