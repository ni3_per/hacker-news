import 'dart:developer' as Developer;

import 'package:logging/logging.dart';

void loggerConfigure() {
  Logger.root.level = Level.ALL; // defaults to Level.INFO
  Logger.root.onRecord.listen((record) {
    Developer.log('${record.level.name} : ${record.time} : ${record.message}');
  });
}

final log = Logger('hacker_news');
