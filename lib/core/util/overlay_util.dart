import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:hacker_news/core/index.dart';

class OverlayUtility {
  static void showToast(
    String message,
    BuildContext context, {
    FlushbarPosition position = FlushbarPosition.BOTTOM,
    Color backgroundColor = Colors.blue,
  }) {
    Flushbar(
      title: null,
      backgroundColor: backgroundColor,
      message: message,
      flushbarPosition: position,
      margin: EdgeInsets.all(10),
      borderRadius: BorderRadius.circular(AppFontSize.value8),
      duration: Duration(seconds: 2),
    )..show(context);
  }
}
