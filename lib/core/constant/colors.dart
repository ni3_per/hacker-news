import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppColors {
  static const MaterialColor kAppPrimarySwatch =
      const MaterialColor(0xFFE65100, const <int, Color>{
    50: const Color(0xFFE65100),
    100: const Color(0xFFE65100),
    200: const Color(0xFFE65100),
    300: const Color(0xFFE65100),
    400: const Color(0xFFE65100),
    500: const Color(0xFFE65100),
    600: const Color(0xFFE65100),
    700: const Color(0xFFE65100),
    800: const Color(0xFFE65100),
    900: const Color(0xFFE65100),
  });
  static const Color kMain = Color(0xFFE65100);
  static const Color kMainSwatch = Color.fromRGBO(210, 75, 75, 1.0);
  static const Color kBrown900 = Color(0xFF3E2723);
  static const Color kInputTextBackground = Color(0xFFF2F2F2);
  static const Color kFontLightColor = Color(0xFF666362);
}
