class AppStrings {
  static _AppFont appFont = _AppFont();
  static _ScreenTitle screenTitle = _ScreenTitle();
  static _HintString hint = _HintString();
  static _LabelString label = _LabelString();
  static _ButtonTitleString button = _ButtonTitleString();
  static _MessageString message = _MessageString();
  static _DateFormatsString dateFormats = _DateFormatsString();
}

class APIs {
  static const String apiBaseUrl = 'https://hn.algolia.com/api/v1/';
  static const String searchNews = 'search?query={query}';
  static const String fetchNewsDetails = 'items/{news_id}';
}

class _AppFont {
  final String kMontserrat = 'Montserrat';
}

class _ScreenTitle {
  final String kHackerNews = "Hacker News";
}

class _HintString {
  final String kEnterSearchValue = "Enter word to search news";
  final String kEnterRequiredValue = "Enter required value";
}

class _LabelString {
  final String kSearch = "Search";
  final String kHackerNews = 'Hacker News';
  final String kSeachNewsMsg =
      'Click search icon to search news by keywords. e.g., Crypto, COVID19, Invesments.';
  final String kNewsDetails = 'News Details';
  final String kNoCommentsFound = 'No comments found';
  final String kComments = 'Comments';
  final String kWelcomeMsg = 'Welcome to Hacker News';
  final String kAppTagLine = 'Because you deserve nothing but the truth';
}

class _ButtonTitleString {
  final String kGetStarted = "Get Started";
}

class _MessageString {
  final String kNoInternet =
      'No Internet.. Please check your internet connectivity';

  final String kSomethingWentWrong = "Something went wrong, Please try again";
}

class _DateFormatsString {
  final String kDayDate = "EEE, MMM d, yyyy";
  final String kDate = "MM/dd/yyyy";
  final String kDateTime = "MM/dd/yyyy hh:mm a";
  final String kFullDayDate = "EEEE, MMM dd, yyyy";
  final String kDayDateTime = "EEEE, hh:mm aaa";
  final String kFullUTCDateTime = "yyyy-MM-ddTHH:mm:ss.SSS'Z'";
}
