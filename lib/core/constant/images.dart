class Images {
  static const String kAvatar = 'assets/images/avatar_logo.png';
  static const String kPlaceHolder = 'assets/images/placeholder_image.png';
  static const String kSplashScreen = 'assets/images/splash_screen_image.png';
}
