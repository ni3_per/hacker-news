import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class SearchNewsEvent extends Equatable {
  const SearchNewsEvent();
}

class SearchNewsInit extends SearchNewsEvent {
  final BuildContext context;
  final String query;

  SearchNewsInit({required this.query, required this.context});
  @override
  List<Object> get props => [];
}

class FetchNewsDetailsInit extends SearchNewsEvent {
  final BuildContext context;
  final String newsId;

  FetchNewsDetailsInit({required this.newsId, required this.context});

  @override
  List<Object> get props => [];
}
