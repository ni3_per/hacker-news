import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/core/webservice/api_client.dart';
import 'package:hacker_news/ui/search_news/entities/news_info.dart';
import 'package:hacker_news/ui/search_news/entities/news_list_info.dart';
import 'package:provider/provider.dart';

import 'bloc.dart';

class SearchNewsBloc extends Bloc<SearchNewsEvent, SearchNewsState> {
  SearchNewsBloc() : super(SearchNewsInitial());

  @override
  Stream<SearchNewsState> mapEventToState(SearchNewsEvent event) async* {
    if (event is SearchNewsInit) {
      yield* searchNewsByQuery(event);
    } else if (event is FetchNewsDetailsInit) {
      yield* fetchNewsDetails(event);
    }
  }

  Stream<SearchNewsState> searchNewsByQuery(SearchNewsInit event) async* {
    {
      try {
        yield SearchNewsLoading();

        final result =
            await Provider.of<ApiClient>(event.context, listen: false)
                .searchNews(event.query);
        yield SearchNewsSuccess(
            newsListInfo: result.body == null
                ? new NewsListInfo()
                : result.body!.toEntity());
      } on ServerException catch (serverException) {
        yield SearchNewsFailed(message: serverException.message!);
      } catch (error) {
        yield SearchNewsFailed(message: AppStrings.message.kSomethingWentWrong);
      }
    }
  }

  Stream<SearchNewsState> fetchNewsDetails(FetchNewsDetailsInit event) async* {
    {
      try {
        yield SearchNewsDetailLoading();

        final result =
            await Provider.of<ApiClient>(event.context, listen: false)
                .fetchNewsDetails(event.newsId);
        yield SearchNewsDetailSuccess(
          newsInfo:
              result.body == null ? new NewsInfo() : result.body!.toEntity(),
        );
      } on ServerException catch (serverException) {
        yield SearchNewsDetailFailed(message: serverException.message!);
      } catch (error) {
        yield SearchNewsDetailFailed(
            message: AppStrings.message.kSomethingWentWrong);
      }
    }
  }
}
