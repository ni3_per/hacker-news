import 'package:equatable/equatable.dart';
import 'package:hacker_news/ui/search_news/entities/news_info.dart';
import 'package:hacker_news/ui/search_news/entities/news_list_info.dart';

abstract class SearchNewsState extends Equatable {
  const SearchNewsState();
}

class SearchNewsInitial extends SearchNewsState {
  @override
  List<Object> get props => [];
}

class SearchNewsLoading extends SearchNewsState {
  @override
  List<Object> get props => [];
}

class SearchNewsFailed extends SearchNewsState {
  final String message;

  SearchNewsFailed({required this.message});

  @override
  List<Object> get props => [message];
}

class SearchNewsSuccess extends SearchNewsState {
  final NewsListInfo newsListInfo;

  SearchNewsSuccess({required this.newsListInfo});

  @override
  List<Object> get props => [newsListInfo];
}

class SearchNewsDetailLoading extends SearchNewsState {
  @override
  List<Object> get props => [];
}

class SearchNewsDetailSuccess extends SearchNewsState {
  final NewsInfo newsInfo;

  SearchNewsDetailSuccess({required this.newsInfo});

  @override
  List<Object> get props => [newsInfo];
}

class SearchNewsDetailFailed extends SearchNewsState {
  final String message;

  SearchNewsDetailFailed({required this.message});

  @override
  List<Object> get props => [message];
}
