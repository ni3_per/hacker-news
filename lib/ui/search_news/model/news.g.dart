// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<News> _$newsSerializer = new _$NewsSerializer();

class _$NewsSerializer implements StructuredSerializer<News> {
  @override
  final Iterable<Type> types = const [News, _$News];
  @override
  final String wireName = 'News';

  @override
  Iterable<Object?> serialize(Serializers serializers, News object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.objectID;
    if (value != null) {
      result
        ..add('objectID')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.title;
    if (value != null) {
      result
        ..add('title')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.url;
    if (value != null) {
      result
        ..add('url')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.author;
    if (value != null) {
      result
        ..add('author')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.points;
    if (value != null) {
      result
        ..add('points')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.storyText;
    if (value != null) {
      result
        ..add('story_text')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.commentText;
    if (value != null) {
      result
        ..add('comment_text')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.numComments;
    if (value != null) {
      result
        ..add('num_comments')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.relevancyScore;
    if (value != null) {
      result
        ..add('relevancy_score')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.createdAt;
    if (value != null) {
      result
        ..add('created_at')
        ..add(serializers.serialize(value,
            specifiedType: const FullType(String)));
    }
    value = object.children;
    if (value != null) {
      result
        ..add('children')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(Comment)])));
    }
    return result;
  }

  @override
  News deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NewsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'objectID':
          result.objectID = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'url':
          result.url = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'author':
          result.author = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'points':
          result.points = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'story_text':
          result.storyText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'comment_text':
          result.commentText = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'num_comments':
          result.numComments = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'relevancy_score':
          result.relevancyScore = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'created_at':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'children':
          result.children.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Comment)]))!
              as BuiltList<Object>);
          break;
      }
    }

    return result.build();
  }
}

class _$News extends News {
  @override
  final String? objectID;
  @override
  final String? title;
  @override
  final String? url;
  @override
  final String? author;
  @override
  final int? points;
  @override
  final String? storyText;
  @override
  final String? commentText;
  @override
  final int? numComments;
  @override
  final int? relevancyScore;
  @override
  final String? createdAt;
  @override
  final BuiltList<Comment>? children;

  factory _$News([void Function(NewsBuilder)? updates]) =>
      (new NewsBuilder()..update(updates)).build();

  _$News._(
      {this.objectID,
      this.title,
      this.url,
      this.author,
      this.points,
      this.storyText,
      this.commentText,
      this.numComments,
      this.relevancyScore,
      this.createdAt,
      this.children})
      : super._();

  @override
  News rebuild(void Function(NewsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NewsBuilder toBuilder() => new NewsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is News &&
        objectID == other.objectID &&
        title == other.title &&
        url == other.url &&
        author == other.author &&
        points == other.points &&
        storyText == other.storyText &&
        commentText == other.commentText &&
        numComments == other.numComments &&
        relevancyScore == other.relevancyScore &&
        createdAt == other.createdAt &&
        children == other.children;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc($jc(0, objectID.hashCode),
                                            title.hashCode),
                                        url.hashCode),
                                    author.hashCode),
                                points.hashCode),
                            storyText.hashCode),
                        commentText.hashCode),
                    numComments.hashCode),
                relevancyScore.hashCode),
            createdAt.hashCode),
        children.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('News')
          ..add('objectID', objectID)
          ..add('title', title)
          ..add('url', url)
          ..add('author', author)
          ..add('points', points)
          ..add('storyText', storyText)
          ..add('commentText', commentText)
          ..add('numComments', numComments)
          ..add('relevancyScore', relevancyScore)
          ..add('createdAt', createdAt)
          ..add('children', children))
        .toString();
  }
}

class NewsBuilder implements Builder<News, NewsBuilder> {
  _$News? _$v;

  String? _objectID;
  String? get objectID => _$this._objectID;
  set objectID(String? objectID) => _$this._objectID = objectID;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _url;
  String? get url => _$this._url;
  set url(String? url) => _$this._url = url;

  String? _author;
  String? get author => _$this._author;
  set author(String? author) => _$this._author = author;

  int? _points;
  int? get points => _$this._points;
  set points(int? points) => _$this._points = points;

  String? _storyText;
  String? get storyText => _$this._storyText;
  set storyText(String? storyText) => _$this._storyText = storyText;

  String? _commentText;
  String? get commentText => _$this._commentText;
  set commentText(String? commentText) => _$this._commentText = commentText;

  int? _numComments;
  int? get numComments => _$this._numComments;
  set numComments(int? numComments) => _$this._numComments = numComments;

  int? _relevancyScore;
  int? get relevancyScore => _$this._relevancyScore;
  set relevancyScore(int? relevancyScore) =>
      _$this._relevancyScore = relevancyScore;

  String? _createdAt;
  String? get createdAt => _$this._createdAt;
  set createdAt(String? createdAt) => _$this._createdAt = createdAt;

  ListBuilder<Comment>? _children;
  ListBuilder<Comment> get children =>
      _$this._children ??= new ListBuilder<Comment>();
  set children(ListBuilder<Comment>? children) => _$this._children = children;

  NewsBuilder();

  NewsBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _objectID = $v.objectID;
      _title = $v.title;
      _url = $v.url;
      _author = $v.author;
      _points = $v.points;
      _storyText = $v.storyText;
      _commentText = $v.commentText;
      _numComments = $v.numComments;
      _relevancyScore = $v.relevancyScore;
      _createdAt = $v.createdAt;
      _children = $v.children?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(News other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$News;
  }

  @override
  void update(void Function(NewsBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$News build() {
    _$News _$result;
    try {
      _$result = _$v ??
          new _$News._(
              objectID: objectID,
              title: title,
              url: url,
              author: author,
              points: points,
              storyText: storyText,
              commentText: commentText,
              numComments: numComments,
              relevancyScore: relevancyScore,
              createdAt: createdAt,
              children: _children?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'children';
        _children?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'News', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
