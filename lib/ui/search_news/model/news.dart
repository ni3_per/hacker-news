import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/ui/search_news/entities/news_info.dart';
import 'package:hacker_news/ui/search_news/model/comment.dart';
import 'package:built_collection/built_collection.dart';

part 'news.g.dart';

abstract class News implements Built<News, NewsBuilder> {
  String? get objectID;

  String? get title;

  String? get url;

  String? get author;

  int? get points;

  @BuiltValueField(wireName: 'story_text')
  String? get storyText;

  @BuiltValueField(wireName: 'comment_text')
  String? get commentText;

  @BuiltValueField(wireName: 'num_comments')
  int? get numComments;

  @BuiltValueField(wireName: 'relevancy_score')
  int? get relevancyScore;

  @BuiltValueField(wireName: 'created_at')
  String? get createdAt;

  BuiltList<Comment>? get children;

  News._();

  factory News([updates(NewsBuilder b)]) = _$News;

  static Serializer<News> get serializer => _$newsSerializer;

  NewsInfo toEntity() {
    final newsInfo = NewsInfo();

    newsInfo.objectID = objectID ?? '';

    newsInfo.title = title ?? 'NA';

    newsInfo.url = url ?? '';

    newsInfo.author = author ?? '';

    newsInfo.points = points ?? 0;

    newsInfo.storyText = storyText ?? '';

    newsInfo.commentText = commentText ?? '';

    newsInfo.numComments = numComments ?? 0;

    newsInfo.relevancyScore = relevancyScore ?? 0;

    newsInfo.createdAt = createdAt ?? '';

    newsInfo.timeAgoString = newsInfo.createdAt.isEmpty
        ? ''
        : Utilities.getTimeAgoString(newsInfo.createdAt);

    newsInfo.children = children == null
        ? []
        : (Utilities.getListOfCommentInfo(children?.asList()));

    return newsInfo;
  }
}
