// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'news_list.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<NewsList> _$newsListSerializer = new _$NewsListSerializer();

class _$NewsListSerializer implements StructuredSerializer<NewsList> {
  @override
  final Iterable<Type> types = const [NewsList, _$NewsList];
  @override
  final String wireName = 'NewsList';

  @override
  Iterable<Object?> serialize(Serializers serializers, NewsList object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    Object? value;
    value = object.hits;
    if (value != null) {
      result
        ..add('hits')
        ..add(serializers.serialize(value,
            specifiedType:
                const FullType(BuiltList, const [const FullType(News)])));
    }
    value = object.page;
    if (value != null) {
      result
        ..add('page')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.hitsPerPage;
    if (value != null) {
      result
        ..add('hitsPerPage')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    value = object.nbPages;
    if (value != null) {
      result
        ..add('nbPages')
        ..add(serializers.serialize(value, specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  NewsList deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new NewsListBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case 'hits':
          result.hits.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, const [const FullType(News)]))!
              as BuiltList<Object>);
          break;
        case 'page':
          result.page = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'hitsPerPage':
          result.hitsPerPage = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'nbPages':
          result.nbPages = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }

    return result.build();
  }
}

class _$NewsList extends NewsList {
  @override
  final BuiltList<News>? hits;
  @override
  final int? page;
  @override
  final int? hitsPerPage;
  @override
  final int? nbPages;

  factory _$NewsList([void Function(NewsListBuilder)? updates]) =>
      (new NewsListBuilder()..update(updates)).build();

  _$NewsList._({this.hits, this.page, this.hitsPerPage, this.nbPages})
      : super._();

  @override
  NewsList rebuild(void Function(NewsListBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  NewsListBuilder toBuilder() => new NewsListBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is NewsList &&
        hits == other.hits &&
        page == other.page &&
        hitsPerPage == other.hitsPerPage &&
        nbPages == other.nbPages;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, hits.hashCode), page.hashCode), hitsPerPage.hashCode),
        nbPages.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('NewsList')
          ..add('hits', hits)
          ..add('page', page)
          ..add('hitsPerPage', hitsPerPage)
          ..add('nbPages', nbPages))
        .toString();
  }
}

class NewsListBuilder implements Builder<NewsList, NewsListBuilder> {
  _$NewsList? _$v;

  ListBuilder<News>? _hits;
  ListBuilder<News> get hits => _$this._hits ??= new ListBuilder<News>();
  set hits(ListBuilder<News>? hits) => _$this._hits = hits;

  int? _page;
  int? get page => _$this._page;
  set page(int? page) => _$this._page = page;

  int? _hitsPerPage;
  int? get hitsPerPage => _$this._hitsPerPage;
  set hitsPerPage(int? hitsPerPage) => _$this._hitsPerPage = hitsPerPage;

  int? _nbPages;
  int? get nbPages => _$this._nbPages;
  set nbPages(int? nbPages) => _$this._nbPages = nbPages;

  NewsListBuilder();

  NewsListBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _hits = $v.hits?.toBuilder();
      _page = $v.page;
      _hitsPerPage = $v.hitsPerPage;
      _nbPages = $v.nbPages;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(NewsList other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$NewsList;
  }

  @override
  void update(void Function(NewsListBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$NewsList build() {
    _$NewsList _$result;
    try {
      _$result = _$v ??
          new _$NewsList._(
              hits: _hits?.build(),
              page: page,
              hitsPerPage: hitsPerPage,
              nbPages: nbPages);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'hits';
        _hits?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'NewsList', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
