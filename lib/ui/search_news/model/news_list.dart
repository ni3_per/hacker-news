import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:built_collection/built_collection.dart';
import 'package:hacker_news/ui/search_news/entities/news_list_info.dart';

import 'news.dart';

part 'news_list.g.dart';

abstract class NewsList implements Built<NewsList, NewsListBuilder> {
  BuiltList<News>? get hits;

  int? get page;

  int? get hitsPerPage;

  int? get nbPages;

  NewsList._();

  factory NewsList([updates(NewsListBuilder b)]) = _$NewsList;

  static Serializer<NewsList> get serializer => _$newsListSerializer;

  NewsListInfo toEntity() {
    final newsListInfo = NewsListInfo();

    newsListInfo.news =
        hits == null ? [] : hits!.map((News news) => news.toEntity()).toList();

    newsListInfo.page = page ?? 0;

    newsListInfo.hitsPerPage = hitsPerPage ?? 0;

    newsListInfo.nbPages = nbPages ?? 0;

    return newsListInfo;
  }
}
