import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/ui/search_news/entities/comment_info.dart';

part 'comment.g.dart';

abstract class Comment implements Built<Comment, CommentBuilder> {
  int? get id;

  String? get type;

  String? get author;

  String? get title;

  String? get url;

  String? get text;

  int? get points;

  @BuiltValueField(wireName: 'parent_id')
  int? get parentId;

  @BuiltValueField(wireName: 'story_id')
  int? get storyId;

  @BuiltValueField(wireName: 'created_at')
  String? get createdAt;

  Comment._();

  factory Comment([updates(CommentBuilder b)]) = _$Comment;

  static Serializer<Comment> get serializer => _$commentSerializer;

  CommentInfo toEntity() {
    final commentInfo = CommentInfo();

    commentInfo.id = id ?? 0;

    commentInfo.type = type ?? '';

    commentInfo.author = author ?? '';

    commentInfo.title = title ?? '';

    commentInfo.url = url ?? '';

    commentInfo.text = text ?? '';

    commentInfo.points = points ?? 0;

    commentInfo.parentId = parentId ?? 0;

    commentInfo.storyId = storyId ?? 0;

    commentInfo.createdAt = (createdAt == null ? '' : createdAt)!;

    commentInfo.timeAgoString = commentInfo.createdAt.isEmpty
        ? ''
        : Utilities.getTimeAgoString(commentInfo.createdAt);

    return commentInfo;
  }
}
