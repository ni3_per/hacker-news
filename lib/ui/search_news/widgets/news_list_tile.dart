import 'package:flutter/material.dart';
import 'package:hacker_news/core/constant/app_font_size.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/ui/search_news/entities/news_info.dart';

import 'new_statistics_widget.dart';

class NewsListTile extends StatelessWidget {
  final NewsInfo newsInfo;

  const NewsListTile({
    required this.newsInfo,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.only(
        top: AppFontSize.value16,
        left: AppFontSize.value16,
        right: AppFontSize.value16,
      ),
      child: Container(
        padding: EdgeInsets.all(AppFontSize.h5),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                  constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.7,
                  ),
                  child: Text(
                    newsInfo.title,
                    style: TextStyles.getH3(
                        Colors.black, FontWeight.w600, FontStyle.normal),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                Spacer(),
                NewsStatisticsWidget(
                    icon: Icons.comment, val: newsInfo.numComments.toString()),
              ],
            ),
            SizedBox(height: AppFontSize.value20),
            Row(
              children: [
                Container(
                    constraints: BoxConstraints(
                      maxWidth: MediaQuery.of(context).size.width * 0.36,
                    ),
                    child: NewsStatisticsWidget(
                        icon: Icons.person, val: newsInfo.author)),
                Spacer(),
                NewsStatisticsWidget(
                  icon: Icons.access_time_rounded,
                  val: newsInfo.timeAgoString,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
