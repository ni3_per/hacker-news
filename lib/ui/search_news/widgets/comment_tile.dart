import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/ui/search_news/entities/comment_info.dart';

class CommentTile extends StatelessWidget {
  final CommentInfo commentInfo;
  const CommentTile({required this.commentInfo, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(AppFontSize.value16),
      decoration: BoxDecoration(
          border: BorderDirectional(bottom: BorderSide(color: Colors.grey))),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CircleAvatar(
            child: Text(commentInfo.author.isEmpty
                ? ''
                : commentInfo.author[0].toUpperCase()),
            maxRadius: AppFontSize.value26,
          ),
          SizedBox(width: AppFontSize.value10),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    commentInfo.author,
                    style: TextStyles.getH4(AppColors.kFontLightColor,
                        FontWeight.w600, FontStyle.normal),
                  ),
                  SizedBox(width: AppFontSize.value6),
                  Text(
                    commentInfo.timeAgoString,
                    style: TextStyles.getH5(AppColors.kFontLightColor,
                        FontWeight.w500, FontStyle.normal),
                  ),
                ],
              ),
              SizedBox(height: AppFontSize.value16),
              Container(
                width: MediaQuery.of(context).size.width -
                    (AppFontSize.value22 * 5),
                child: Html(
                  shrinkWrap: true,
                  data: "${commentInfo.text}",
                  style: {
                    "p": Style(
                      padding: EdgeInsets.all(0),
                      margin: EdgeInsets.all(0),
                    ),
                  },
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
