import 'package:flutter/material.dart';
import 'package:hacker_news/core/index.dart';

class NewsStatisticsWidget extends StatelessWidget {
  const NewsStatisticsWidget({
    required this.val,
    required this.icon,
    Key? key,
  }) : super(key: key);

  final String val;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: AppColors.kFontLightColor,
          size: AppFontSize.value28,
        ),
        SizedBox(width: AppFontSize.value4),
        Text(
          val,
          style: TextStyles.getH5(
              AppColors.kFontLightColor, FontWeight.w500, FontStyle.normal),
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
        ),
      ],
    );
  }
}
