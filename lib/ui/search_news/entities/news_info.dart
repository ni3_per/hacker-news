import 'dart:collection';

import 'package:hacker_news/ui/search_news/entities/comment_info.dart';

class NewsInfo {
  late String objectID;

  late String title;

  late String url;

  late String author;

  late int points;

  late String storyText;

  late String commentText;

  late int numComments;

  late int relevancyScore;

  late String createdAt;

  late String timeAgoString;

  late List<CommentInfo> children;

  Map<String, dynamic> toMap() {
    return {
      'objectID': objectID,
      'title': title,
      'url': url,
      'author': author,
      'points': points,
      'storyText': storyText,
      'commentText': commentText,
      'numComments': numComments,
      'relevancyScore': relevancyScore,
      'createdAt': createdAt,
      'children': children,
      'timeAgoString': timeAgoString,
    };
  }

  NewsInfo fromJson(LinkedHashMap<String, dynamic> newsInfo) {
    NewsInfo news = new NewsInfo();

    news.objectID = newsInfo['objectID'];
    news.title = newsInfo['title'];
    news.url = newsInfo['url'];
    news.author = newsInfo['author'];
    news.points = newsInfo['points'];
    news.storyText = newsInfo['storyText'];
    news.commentText = newsInfo['commentText'];
    news.numComments = newsInfo['numComments'];
    news.relevancyScore = newsInfo['relevancyScore'];
    news.createdAt = newsInfo['createdAt'];
    news.timeAgoString = newsInfo['timeAgoString'];

    return news;
  }
}
