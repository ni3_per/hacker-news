import 'dart:collection';

import 'news_info.dart';

class NewsListInfo {
  late List<NewsInfo> news;

  late int page;

  late int hitsPerPage;

  late int nbPages;

  Map<String, dynamic> toMap() {
    return {
      'news': news,
      'page': page,
      'hitsPerPage': hitsPerPage,
      'nbPages': nbPages,
    };
  }

  NewsListInfo fromJson(LinkedHashMap<String, dynamic> newsListInfo) {
    NewsListInfo newsList = new NewsListInfo();

    newsList.news = newsListInfo['news'];
    newsList.page = newsListInfo['page'];
    newsList.hitsPerPage = newsListInfo['hitsPerPage'];
    newsList.nbPages = newsListInfo['nbPages'];

    return newsList;
  }
}
