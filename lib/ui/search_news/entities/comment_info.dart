import 'dart:collection';

class CommentInfo {
  late int id;

  late String type;

  late String author;

  late String title;

  late String url;

  late String text;

  late int points;

  late int parentId;

  late int storyId;

  late String createdAt;

  late String timeAgoString;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'type': type,
      'author': author,
      'url': url,
      'title': title,
      'text': text,
      'points': points,
      'parentId': parentId,
      'storyId': storyId,
      'createdAt': createdAt,
      'timeAgoString': timeAgoString,
    };
  }

  CommentInfo fromJson(LinkedHashMap<String, dynamic> commentInfo) {
    CommentInfo comment = new CommentInfo();

    comment.id = commentInfo['id'];
    comment.type = commentInfo['type'];
    comment.author = commentInfo['author'];
    comment.title = commentInfo['title'];
    comment.url = commentInfo['url'];
    comment.text = commentInfo['text'];
    comment.points = commentInfo['points'];
    comment.parentId = commentInfo['parentId'];
    comment.storyId = commentInfo['storyId'];
    comment.createdAt = commentInfo['createdAt'];
    comment.timeAgoString = commentInfo['timeAgoString'];

    return comment;
  }
}
