import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/core/util/overlay_util.dart';
import 'package:hacker_news/ui/comman_widget/loading_indicator.dart';
import 'package:hacker_news/ui/search_news/bloc/bloc.dart';
import 'package:hacker_news/ui/search_news/entities/comment_info.dart';
import 'package:hacker_news/ui/search_news/widgets/comment_tile.dart';
import 'package:hacker_news/ui/search_news/widgets/new_statistics_widget.dart';

class NewsDetailScreen extends StatefulWidget {
  final String newsID;
  NewsDetailScreen({required this.newsID, Key? key}) : super(key: key);

  @override
  NewsDetailScreenState createState() => NewsDetailScreenState();
}

class NewsDetailScreenState extends State<NewsDetailScreen> {
  late SearchNewsBloc _searchNewsBloc;

  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback(_onLayoutDone);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchNewsBloc, SearchNewsState>(
      listener: _searchNewsBlocListner,
      child: BlocBuilder<SearchNewsBloc, SearchNewsState>(
        builder: (context, state) {
          if (state is SearchNewsDetailLoading) {
            return LoadingIndicator();
          } else if (state is SearchNewsDetailSuccess) {
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(AppFontSize.value16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        state.newsInfo.title,
                        style: TextStyles.getH3(
                            Colors.black, FontWeight.w600, FontStyle.normal),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: AppFontSize.value10),
                      Row(children: [
                        NewsStatisticsWidget(
                            icon: Icons.person, val: state.newsInfo.author),
                        Spacer(),
                        NewsStatisticsWidget(
                          icon: Icons.access_time_rounded,
                          val: state.newsInfo.timeAgoString,
                        ),
                      ]),
                    ],
                  ),
                ),
                state.newsInfo.children.isEmpty
                    ? Container()
                    : Padding(
                        padding: EdgeInsets.all(AppFontSize.value16),
                        child: Text(
                          AppStrings.label.kComments,
                          style: TextStyles.getH2(
                              Colors.black, FontWeight.w600, FontStyle.normal),
                        ),
                      ),
                state.newsInfo.children.isEmpty
                    ? Container(
                        padding: EdgeInsets.all(AppFontSize.value16),
                        child: Text(
                          AppStrings.label.kNoCommentsFound,
                          style: TextStyles.getH2(AppColors.kFontLightColor,
                              FontWeight.w500, FontStyle.normal),
                        ))
                    : Expanded(
                        child: ListView.builder(
                          shrinkWrap: true,
                          itemBuilder: (context, index) {
                            CommentInfo commentInfo =
                                state.newsInfo.children[index];
                            return CommentTile(commentInfo: commentInfo);
                          },
                          itemCount: state.newsInfo.children.length,
                        ),
                      )
              ],
            );
          }
          return Container();
        },
      ),
    );
  }

  void _onLayoutDone(Duration timeStamp) {
    _searchNewsBloc = BlocProvider.of<SearchNewsBloc>(context);
    _searchNewsBloc
        .add(FetchNewsDetailsInit(context: context, newsId: widget.newsID));
  }

  void _searchNewsBlocListner(BuildContext context, SearchNewsState state) {
    log.info('-------STATE------- $state');
    if (state is SearchNewsDetailFailed) {
      OverlayUtility.showToast(state.message, context,
          backgroundColor: Colors.red);
    }
  }
}
