import 'dart:async';

import 'package:animations/animations.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hacker_news/core/constant/app_font_size.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/core/util/overlay_util.dart';
import 'package:hacker_news/ui/comman_widget/loading_indicator.dart';
import 'package:hacker_news/ui/search_news/bloc/bloc.dart';
import 'package:hacker_news/ui/search_news/entities/news_info.dart';

import 'package:hacker_news/ui/search_news/screens/news_details_page.dart';
import 'package:hacker_news/ui/search_news/widgets/news_list_tile.dart';

class SearchNewsScreen extends StatefulWidget {
  SearchNewsScreen({Key? key}) : super(key: key);

  @override
  SearchNewsScreenState createState() => SearchNewsScreenState();
}

class SearchNewsScreenState extends State<SearchNewsScreen> {
  late SearchNewsBloc _searchNewsBloc;
  bool _isSearchBarVisible = false;
  TextEditingController _searchController = TextEditingController();
  FocusNode? _searchFocusNode = FocusNode();
  Timer? _debounce;

  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback(_onLayoutDone);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SearchNewsBloc, SearchNewsState>(
      listener: _searchNewsBlocListner,
      child: Column(
        children: [
          buildSearchInputBar(),
          Expanded(
            child: BlocBuilder<SearchNewsBloc, SearchNewsState>(
              builder: (context, state) {
                if (state is SearchNewsLoading) {
                  return LoadingIndicator();
                } else if (state is SearchNewsSuccess) {
                  return ListView.builder(
                    itemBuilder: (context, index) {
                      NewsInfo _newsInfo = state.newsListInfo.news[index];
                      return OpenContainer(
                        closedColor: AppColors.kInputTextBackground,
                        openColor: Colors.blue,
                        closedElevation: 0.0,
                        openElevation: 0.0,
                        closedShape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        ),
                        transitionType: ContainerTransitionType.fadeThrough,
                        transitionDuration: const Duration(milliseconds: 600),
                        openBuilder: (context, action) {
                          return NewsDetailsPage(
                            newsID: _newsInfo.objectID,
                          );
                        },
                        onClosed: (Never? never) {
                          log.info('CLOSEd');
                          if (mounted) {
                            setState(() {
                              _isSearchBarVisible = false;
                            });
                            _toggleKeyboard();
                          }
                        },
                        closedBuilder: (context, action) {
                          return NewsListTile(
                            newsInfo: _newsInfo,
                          );
                        },
                      );
                    },
                    itemCount: state.newsListInfo.news.length,
                  );
                }
                return Center(
                  child: _searchController.text.isNotEmpty
                      ? Container()
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.search_rounded,
                              color: AppColors.kFontLightColor,
                              size: AppFontSize.value30 * 1.6,
                            ),
                            SizedBox(height: AppFontSize.value6),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.85,
                              child: Text(
                                AppStrings.label.kSeachNewsMsg,
                                textAlign: TextAlign.center,
                                style: TextStyles.getH1(
                                    AppColors.kFontLightColor,
                                    FontWeight.w500,
                                    FontStyle.normal),
                              ),
                            ),
                          ],
                        ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void _onLayoutDone(Duration timeStamp) {
    _searchNewsBloc = BlocProvider.of<SearchNewsBloc>(context);
    // _searchNewsBloc.add(SearchNewsInit(query: 'hackernews', context: context));
  }

  void _searchNewsBlocListner(BuildContext context, SearchNewsState state) {
    log.info('-------STATE------- $state');
    if (state is SearchNewsFailed) {
      OverlayUtility.showToast(state.message, context,
          backgroundColor: Colors.red);
    }
  }

  void onSearchNews() {
    _searchController.text = '';
    setState(() {
      _isSearchBarVisible = !_isSearchBarVisible;
    });
    _toggleKeyboard();
  }

  void _toggleKeyboard() {
    if (_isSearchBarVisible) {
      FocusScope.of(context).requestFocus(_searchFocusNode);
    } else {
      FocusScope.of(context).requestFocus(FocusNode());
    }
  }

  void _onSearchChanged(String query) {
    setState(() {
      if (_debounce?.isActive ?? false) _debounce?.cancel();
      _debounce = Timer(const Duration(milliseconds: 1000), () {
        if (query.isEmpty) {
          return;
        }
        _searchNewsBloc.add(SearchNewsInit(query: query, context: context));
      });
    });
  }

  AnimatedContainer buildSearchInputBar() {
    return AnimatedContainer(
      height: _isSearchBarVisible ? 40 : 0,
      duration: Duration(milliseconds: 200),
      color: AppColors.kInputTextBackground,
      alignment: Alignment.center,
      child: Center(
        child: TextField(
          controller: _searchController,
          focusNode: _searchFocusNode,
          maxLines: 1,
          decoration: TextStyles.getInputDecoration(),
          onChanged: _onSearchChanged,
        ),
      ),
    );
  }
}
