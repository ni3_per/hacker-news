import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/core/webservice/api_client.dart';
import 'package:hacker_news/ui/search_news/bloc/bloc.dart';
import 'package:provider/provider.dart';

class SearchNewsPage extends StatelessWidget {
  final GlobalKey<SearchNewsScreenState> _globalKey =
      GlobalKey<SearchNewsScreenState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.kInputTextBackground,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          AppStrings.label.kHackerNews,
          style:
              TextStyles.getH2(Colors.white, FontWeight.w600, FontStyle.normal),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.search),
        onPressed: _onPressSearch,
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: BlocProvider<SearchNewsBloc>(
          create: (_) => SearchNewsBloc(),
          child: Provider(
              create: (_) => ApiClient.create(),
              dispose: (_, ApiClient service) => service.client.dispose(),
              child: SearchNewsScreen(
                key: _globalKey,
              )),
        ),
      ),
    );
  }

  void _onPressSearch() {
    _globalKey.currentState?.onSearchNews();
  }
}
