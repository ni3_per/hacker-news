import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hacker_news/core/index.dart';
import 'package:hacker_news/core/webservice/api_client.dart';
import 'package:hacker_news/ui/search_news/bloc/bloc.dart';
import 'package:provider/provider.dart';

import 'news_details_screen.dart';

class NewsDetailsPage extends StatelessWidget {
  final String newsID;
  NewsDetailsPage({required this.newsID, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.kInputTextBackground,
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          AppStrings.label.kNewsDetails,
          style:
              TextStyles.getH2(Colors.white, FontWeight.w600, FontStyle.normal),
        ),
      ),
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: BlocProvider<SearchNewsBloc>(
          create: (_) => SearchNewsBloc(),
          child: Provider(
              create: (_) => ApiClient.create(),
              dispose: (_, ApiClient service) => service.client.dispose(),
              child: NewsDetailScreen(
                newsID: newsID,
              )),
        ),
      ),
    );
  }
}
