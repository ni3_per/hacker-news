import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hacker_news/core/constant/app_font_size.dart';
import 'package:hacker_news/core/constant/index.dart';
import 'package:hacker_news/core/webservice/api_client.dart';
import 'package:hacker_news/ui/search_news/bloc/bloc.dart';
import 'package:provider/provider.dart';

import 'core/index.dart';

void main() {
  ApiClient.create();
  loggerConfigure();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SearchNewsBloc(),
      child: Provider(
        create: (_) => ApiClient.create(),
        dispose: (_, ApiClient service) => service.client.dispose(),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: AppStrings.label.kHackerNews,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: MyHomePage(),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          margin: EdgeInsets.all(AppFontSize.value20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                AppStrings.label.kWelcomeMsg,
                textAlign: TextAlign.center,
                style: TextStyles.getH6(
                        Colors.black, FontWeight.w500, FontStyle.normal)
                    .copyWith(fontSize: AppFontSize.value28),
              ),
              SizedBox(height: AppFontSize.value6),
              Text(
                AppStrings.label.kAppTagLine,
                textAlign: TextAlign.center,
                style: TextStyles.getH4(
                    Colors.black54, FontWeight.w400, FontStyle.italic),
              ),
              SizedBox(height: AppFontSize.value22),
              TextButton(
                onPressed: _onPress,
                style: ButtonStyle(
                    padding: MaterialStateProperty.all(
                        EdgeInsets.all(AppFontSize.value12)),
                    backgroundColor: MaterialStateProperty.all(Colors.blue)),
                child: Text(
                  AppStrings.button.kGetStarted,
                  textAlign: TextAlign.center,
                  style: TextStyles.getH3(
                      Colors.white, FontWeight.w500, FontStyle.normal),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onPress() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => SearchNewsPage()));
  }
}
